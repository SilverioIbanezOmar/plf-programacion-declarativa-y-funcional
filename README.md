# Omar Silverio Ibañez PLF ISA

## programación declarativa. Orientadas y pautas para el estudio
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .node {
    BackgroundColor lightblue
  }  
  :depth(1){
    BackgroundColor green
  }
  :depth(2){
    BackgroundColor yellow
  }
  :depth(3){
    BackgroundColor lightgreen
  }
}
</style>
*[#Red] programación declarativa. Orientadas y pautas para el estudio

** partes en la programación
*** Parte creativa
****_ tenemos
***** La idea
***** el algoritmo
***** formas de ahorrar
****** tiempo
****** memoria
****_ lo realiza el 
***** arquitecto(programador)

*** Parte burocratica
****_ se realiza
***** gestión detallada de la memoria.
****_ se utiliza para 
***** seguir la secuencia de ordenes 
***** se ejecute correctamente.
**** Asigna
*****_ al
****** programador la parte del arquitecto

*** Este equilibrio depende del tipo de progrmación
*** Programación declarativa
****_ Hace enfasis
***** Aspectos creativos.
**** Ordenador
***** resuelve los problemas de gestión.
***** Toma perspectivas con mejor explicaciones.

**_ se libra
*** Detallar el control de gestión de memoria.
*** Utiliza otros reursos
*** Complejidad en los programas.
*** Errores haciendo programas.
*** farragosidad del codigo.

**_ ventajas
*** Programas mas cortos
*** Programas mas faciles de realizar
*** Faciles de depurar
*** Las tareas rutinarias en programación.
**** Se dejan al compilador
*** Facilita la programación

**_ caracteristicas
*** Idea abstracta, cada quien programa diferente.
*** Especifica los programas a nivel alto.
**** Lenguaje humano
**** Cercano a la forma de pensar del programador.


** Variantes principales

*** Programación funcional

****_ Recurre al
***** Lenguaje de los matematicos
***** Modelo de los matematicos.

**** Lenguaje
***** cuando describen 
****** funciones
*******_ Utiliza
******** Razonamiento ecuacional
******** Función
********* Se realiza mediante ecuaciones
********* Son utiles para hacer programas
********* Programa
********** Datos de entrada(ARGUMENTOS)
********** Datos de salida (Resultado de la función)
******** Sin ninguna gestión de memoria.

****_ ventajas
***** Tiempo menor
******_ en
******* modificación
******* depuración
***** facil de leer.
***** Todo lo complejo se vuelve natural.
***** programar
******_ de forma
******* sencilla
******* Placentera
***** Funciones de orden superior
****** Funciones actuan sobre otras funciones
****** Capacidad mas potente

**** Evaluación peresoza
***** Las instrucciones no se realizan
***** Hasta que otros no la solicitan.
***** Evalua lo necesario
***** Despues hace calculos posteriores
***** Define tipos de datos infinitos

****_ se reliza mediante
***** Reglas de escritura.
***** Simplificación de expresiones.

**** Existen funciones de orden superior.

*** Programación logica

**** Modelo
***** demuestra expresiones logicas.
***** Expresiones en los programas
****** Axiomas
****** Reglas de inferencia

****_ caracteristicas
***** Logica de predicados de primer orden
*****_ relaciones entre
****** Objetos
****** Predicados
****** Relaciones
*******_ No establecen un orden entre
******** Datos de entrada
******** Datos de salida
***** Permite ser mas declarativo
***** Con un programa podemos realizar muchos programas.

**** Interprete
*****_ dado un conjunto de
****** Relaciones
****** Predicados
****** opera mediante algoritmos de resolución
****** intenta demostrar los predicados

**** compilador
***** Es el motor de inferencia.
****** Razona
****** da respuesta.

**** Relaciones
***** IA
***** Prolog

**** Razonamientos
*****_ sobre
****** Hechos
****** Reglas
@endmindmap
```
___
## Lenguaje de Programación Funcional
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .node {
    BackgroundColor lightblue
  }  
  :depth(1){
    BackgroundColor green
  }
  :depth(2){
    BackgroundColor yellow
  }
  :depth(3){
    BackgroundColor lightgreen
  }
}
</style>
*[#Red] Lenguaje de Programación funcional

** Paradigmas de programación
*** modelo de computación
****_ en el que 
***** Diferentes lenguajes
***** Establecen la semantica
***_ Tipos de
**** Paradigmas
***** POO
***** Programación imperativa

** Programación funcional
***  Bases de la programación funcional
**** Lambda calculo
***** Desarrollado por Haskell 
***** Es una variante logica
***** Peter landing
****** Establecio que todo esto permite
******* Formar un lenguaje de programación

*** Programa en programación funcional
**** son Matematicas
**** son Funciones

***_ caracteristicas
**** Asignación
***** se pierde este concepto
***** Por tanto se pierden los bucles
**** Utiliza recursión
***** Funciones que hacen referencia asi mismas
**** Variables
***** Se refiere a los parametros de entrada
**** Constantes
***** equipan las funciones
***** Si una función devuelve el mismo resultado
****** es constante
**** independencia
***** de los parametros de entrada.

*** Función
**** Todo gira al rededor de esta
**** Que no reciba parametros
***** envia siempre el mismo resultado
***** equiparar con una función constante
**** Función map
***** Recibe una función como parametro
**** Función de orden superior
***** Recibe otra función como parametro de entrada
**** Salida de la función
***** es otra función
***** Todas las funciones pueden devolver funciones
**** Currificación
***** No solo devuelve una salida
***** Devuelve diferentes
****** Funciones
****** Salidas

***_ ventajas
**** Los programas
*****_ dependen de
****** Parametros de entrada
**** Los mismos parametros de entradas
***** devuelven el mismo valor
**** Transparencia referencial
*****_ son
****** Independientes al orden
**** Orden indispensable
**** Podemos paralelizar sin ningún problema

*** Aplicación parcial
**** Una operación se aplica
***** Totalmente
***** Parcialmente
***** a todos sus parametros

*** Evaluaciones
**** Evaluación impaciente o estructurada
***** Hasta que no se evalue lo mas interno
***** No se evalua lo mas externo
***** Es sencilla de implementar
***** Sino se evalua en tiempo finito
****** No se realiza el calculo
****** Produce errores
**** Evaluación no estricta
***** evalua desde fuera hacia adentro
***** es mas complicada de implementar
***** No necesita conocer los valores de sus parametros
***** Memorización
****** almacena el valor de una expresión que ya fue calculada
****** Evaluación perezosa
******* Recuerda lo que ya se ha realizado
******* No trabaja de mas

*** Utilización
**** Videojuegos
**** Prototipos rapidos
**** Tiene eficacia
***** Programas rapidos
***** Programas cortos
**** Coste computacional mayor
@endmindmap
```
